using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using gv = Dana_Inspection.GlobalVariables;
using gc = Dana_Inspection.GlobalOpClass;
using System.Windows.Forms;
using System.IO;
using System.Configuration;
using System.Data;
using System.Diagnostics;

namespace Dana_Inspection
{
    public partial class DataEntry : Form
    {
        public static DataEntry form;
        readonly UserCode uc = new UserCode();

        int counter = 0;
        int[] textEnter = new int[6]; // If you add a TextBox(), you must update this array count.
        public static bool defectFlag;
        public static List<string> defects = new List<string>();
        public bool finalValidate;
        public DateTime inactiveTimeStamp = DateTime.Now.AddMilliseconds(gv.usageInterval);

        private void UserDocMenuItem_Click(object sender, EventArgs e)
        {
             string[] docDir = Directory.GetFiles(@ConfigurationManager.AppSettings["PDF_Documentation"].ToString(), "*.pdf", SearchOption.AllDirectories);
            
            foreach (var doc in docDir)
            {
                Process.Start(new ProcessStartInfo("cmd", $"/c {gv.stringParse(doc)}") { CreateNoWindow = true });
            }
        }

        private void aboutMenuItem_Click(object sender, EventArgs e)
        {
            aboutToolTip.Show("\nUse DIA to enter gage measurement data to the database.\n\nUsers will need to login before use.\nThe interface is touch free during scanning.\nUsers will be logged out after " + ConfigurationManager.AppSettings["LoginInterval"].ToString() + " milliseconds of no activity.\n\n" + @"The ""Help > Documentation"" link will provide you with end-user instructions on how to use the application." + "\n\nPlease contact IT if you experience abnormalalies of DIA.", form);
        }

        private string[] DataParams()
        {
            string defectList = string.Join(", ", defects);
            string[] username = { UserLbl.Text };
            gv.passcodes[0] = gc.SQLData("SELECT Passcode FROM UserCodes WHERE Username = @param1", username).Rows[0][0].ToString();

            string[] dataParams = { BarCodeTxtBox.Text, defectFlag.ToString(), defectList, GageTxtBox1.Text, GageTxtBox2.Text, GageTxtBox3.Text, GageTxtBox4.Text, GageTxtBox5.Text, gv.passcodes[0], DateTime.Now.ToString(), gc.ConvertToJulian(DateTime.Now), gc.ConvertTime(DateTime.Now) };

            if (dataParams[0] == "")
            {
                dataParams[0] = gv.barcode;

                gv.barcode = null;
            }

            return dataParams;
        }

        public TextBox[] TextBoxes([Optional] TextBox name, [Optional] TextBox clearName)
        {
            TextBox[] textBoxes = { BarCodeTxtBox, GageTxtBox1, GageTxtBox2, GageTxtBox3, GageTxtBox4, GageTxtBox5 };

            // Set Validation Error Counter
            if (name != null)
            {
                for (int i = 0; i < textBoxes.Count() - 1; i++)
                {
                    if (textBoxes[i] == name)
                    {
                        textEnter[i] += 1;
                    }
                }
            }

            // Reset Validiation Error Counter
            if (clearName != null)
            {
                for (int i = 0; i < textBoxes.Count() - 1; i++)
                {
                    if (textBoxes[i] == clearName)
                    {
                        textEnter[i] = 0;
                        counter = 0;
                    }
                }
            }

            return textBoxes;
        }

        private Boolean GoodPart(TextBox value, string tableName)
        {
            double dValue;
            string[] valStart = ConfigurationManager.AppSettings["ValidateStart"].Split(", ");
            string[] valEnd = ConfigurationManager.AppSettings["ValidateEnd"].Split(", ");

            // Set usage timestamp
            inactiveTimeStamp = DateTime.Now.AddMilliseconds(gv.usageInterval);
            // Use for Debugging
            //MessageBox.Show("Logout: " + inactiveTimeStamp.ToString());


            // Barcode Validation
            try
            {
                DataTable valRange = gc.SQLData("SELECT StartRange, EndRange FROM " + tableName);

                if (valRange != null)
                {
                    if (value.Text.Contains(valRange.Rows[0][0].ToString()))
                    {
                        // Reset Validation Error Counter
                        TextBoxes(null, value);

                        return true;
                    }
                }
                else
                {
                    throw new Exception("DB Barcode Validation Failure.");
                }
            }
            catch
            {
                if (value.Text.Contains(valStart[0]))
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }
            }


            try
            {
                dValue = double.Parse(value.Text.Trim());

                // Gage Validation
                try
                {
                    DataTable valRange = gc.SQLData("SELECT StartRange, EndRange FROM " + tableName);

                    if (valRange != null)
                    {
                        // Loop through DB validation
                        for (int i = 1; i < valRange.Rows.Count; i++)
                        {
                            double dValStart = double.Parse(valRange.Rows[i][0].ToString());
                            double dValEnd = double.Parse(valRange.Rows[i][1].ToString());

                            // Validation
                            if (dValue > dValStart && dValue < dValEnd)
                            {
                                // Reset Validation Error Counter
                                TextBoxes(null, value);

                                return true;
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("DB Gage Validation Failure.");
                    }
                }
                catch
                {
                    // Loop through DB validation
                    for (int i = 1; i < valStart.Count(); i++)
                    {
                        double dValStart = double.Parse(valStart[i]);
                        double dValEnd = double.Parse(valEnd[i]);

                        // Validation
                        if (dValue > dValStart && dValue < dValEnd)
                        {
                            // Reset Validation Error Counter
                            TextBoxes(null, value);

                            return true;
                        }
                    }
                }


                // Gage Validation
                /*if (dValue > 59.099 && dValue < 59.151)
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }

                if (dValue > 80.159 && dValue < 80.259)
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }

                if (dValue > 69.769 && dValue < 69.819)
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }

                if (dValue > 46.957 && dValue < 46.997)
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }

                if (dValue > 37.879 && dValue < 38.079)
                {
                    // Reset Validation Error Counter
                    TextBoxes(null, value);

                    return true;
                }*/
            }
            catch
            { }

            // Set Validation Error Counter
            TextBoxes(value);

            return false;
        }

        private void BadPart(TextBox textBox)
        {
            for (int i = 0; i < textEnter.Count() - 1; i++)
            {
                if ((i != 0) && (textEnter[i - 1] == 0 && textEnter[i] != 0))
                {
                    counter += 1;
                }
            }

            if (textBox == BarCodeTxtBox)
            {
                uc.badBarcode = true;
            }
            else
            {
                uc.badPart = true;
            }
            uc.ShowDialog();

            textBox.SelectAll();

            if (textBox != BarCodeTxtBox && counter == 2)
            {
                defectFlag = true;

                finalValidate = true;

                SubmitRecord();

                counter = 0;
            }
            
        }

        private void FinalValidation()
        {
            foreach (TextBox tBox in TextBoxes())
            {
                if (GoodPart(tBox, "InspectValidation") == true)
                {
                    finalValidate = true;
                }
                else
                {
                    finalValidate = false;

                    BadPart(tBox);
                }
            }
        }

        private void SubmitRecord()
        {
            if (finalValidate == true)
            {
                gc.SQLString("INSERT INTO " + gv.environment + "Inspection (" + gv.InspectFields + ") VALUES (@param1, @param2, @param3, @param4, @param5, @param6, @param7, @param8, @param9, @param10, @param11, @param12)", DataParams());
            }

            Reload(TextBoxes());
        }

        private void Reload(TextBox[] textBoxes)
        {
            foreach (TextBox textBox in textBoxes)
            {
                textBox.Clear();
            }

            textBoxes[0].Select();

            defects.Clear();
            defectFlag = false;
        }

        public void SubmitDefect()
        {
            finalValidate = true;
            SubmitRecord();
        }

        public void msg(string message, [Optional] string caption)
        {
            MessageBox.Show(message, caption);
        }

        public DataEntry()
        {
            form = this;
            InitializeComponent();
        }

        private void DataEntry_Load(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Maximized;

            uc.MCodeFunction = "Login";

            ClockTimer.Start();

            ActiveControl = BarCodeTxtBox;
        }

        private void BarCodeTxtBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                aboutToolTip.Hide(form);

                if (GoodPart(BarCodeTxtBox, "InspectValidation") == true)
                {
                    GageTxtBox1.Focus();
                }
                else
                {
                    BadPart(BarCodeTxtBox);
                }
            }

        }

        private void FlagBttn_Click(object sender, EventArgs e)
        {
            // Data lost in Memory.
            try
            {
                gv.barcode = TextBoxes()[0].Text;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                gc.ErrorLogger("DB", gc.AppName, ex.Message, ex.StackTrace);
            }

            // Show Defect Reasons Dialog
            Defect_Reasons dr = new Defect_Reasons();
            dr.Show();

            defects.Clear();
        }

        private void GageTxtBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (GoodPart(GageTxtBox1, "InspectValidation") == true)
                {
                    GageTxtBox2.Focus();
                }
                else
                {
                    BadPart(GageTxtBox1);
                }
            }

        }

        private void GageTxtBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (GoodPart(GageTxtBox2, "InspectValidation") == true)
                {
                    GageTxtBox3.Focus();
                }
                else
                {
                    BadPart(GageTxtBox2);
                }
            }

        }

        private void GageTxtBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               if (GoodPart(GageTxtBox3, "InspectValidation") == true)
                {
                    GageTxtBox4.Focus();
                }
                else
                {
                    BadPart(GageTxtBox3);
                }
            }
        }

        private void GageTxtBox4_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               if (GoodPart(GageTxtBox4, "InspectValidation") == true)
                {
                    GageTxtBox5.Focus();
                }
                else
                {
                    BadPart(GageTxtBox4);
                }
            }
        }

        private void GageTxtBox5_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
               if (GoodPart(GageTxtBox5, "InspectValidation") == true)
                {
                    // Final Validation of All Textboxes
                    FinalValidation();

                    SubmitRecord();
                }
                else
                {
                    BadPart(GageTxtBox5);
                }
            }
        }

        private void UserLbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Reload(TextBoxes());

            gv.username = null;

            uc.MCodeFunction = "Login";
            uc.ShowDialog();

            // Upload DB backup file to Database
            if (gc.CheckForInternetConnection() == true && File.Exists(ConfigurationManager.AppSettings["DB_Backup"] + "Inspection_Data.csv"))
            {
                gc.UploadBackup(ConfigurationManager.AppSettings["DB_Backup"], "Inspection");
            }
        }

        private void EditEntryBttn_Click(object sender, EventArgs e)
        {
            UserCode uc = new UserCode();
            uc.StartPosition = FormStartPosition.CenterScreen;
            uc.MCodeFunction = "Edit";
            uc.Show();

            Reload(TextBoxes());
        }

        private void DataEntry_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (uc.MCodeFunction != "Exit" && e.CloseReason == CloseReason.UserClosing)
            {
                e.Cancel = true;

                ExitBttn.PerformClick();
            }
        }

        private void ExitBttn_Click(object sender, EventArgs e)
        {
            uc.MCodeFunction = null;
            uc.MCodeFunction = "Exit";
            uc.ShowDialog();            
        }

        private void ClockTimer_Tick(object sender, EventArgs e)
        {
            if (gv.username == null || gv.username == "Login" || DateTime.Now.CompareTo(inactiveTimeStamp) > 0)
            {
                gv.username = "Login";

                if (uc.Visible == false)
                {
                    uc.MCodeFunction = "Login";
                    uc.ShowDialog();

                    Reload(TextBoxes());
                }
            }

            if (TextBoxes()[0].Text == "")
            {
                TextBoxes()[0].Select();
            }

            UserLbl.Text = gv.username;

            TimeLbl.Text = DateTime.Now.ToString();
        }

    }
}
