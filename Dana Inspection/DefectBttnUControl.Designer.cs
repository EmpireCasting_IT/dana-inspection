﻿
namespace Dana_Inspection
{
    partial class DefectBttnUControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.FaultyReasonBttn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // FaultyReasonBttn
            // 
            this.FaultyReasonBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FaultyReasonBttn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FaultyReasonBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.FaultyReasonBttn.Location = new System.Drawing.Point(0, 0);
            this.FaultyReasonBttn.Name = "FaultyReasonBttn";
            this.FaultyReasonBttn.Size = new System.Drawing.Size(150, 100);
            this.FaultyReasonBttn.TabIndex = 0;
            this.FaultyReasonBttn.Text = "Placeholder";
            this.FaultyReasonBttn.UseVisualStyleBackColor = true;
            this.FaultyReasonBttn.Click += new System.EventHandler(this.FaultyReasonBttn_Click);
            // 
            // DefectBttnUControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.FaultyReasonBttn);
            this.Name = "DefectBttnUControl";
            this.Size = new System.Drawing.Size(150, 100);
            this.Load += new System.EventHandler(this.FaultyBttnUControl_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button FaultyReasonBttn;
    }
}
