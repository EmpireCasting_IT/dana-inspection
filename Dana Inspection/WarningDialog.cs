﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace Dana_Inspection
{
    public partial class WarningDialog : UserControl
    {
        string contentNameParam = "";

        public WarningDialog()
        {
            this.contentNameParam = "Entry is out of range.\nPlease re-measure.";
            InitializeComponent();
        }

        public WarningDialog(string contentName)
        {
            this.contentNameParam = contentName;
            InitializeComponent();
        }

        private void WarningDialog_Load(object sender, EventArgs e)
        {
            AlertLbl.Text = contentNameParam;
        }
    }
}
