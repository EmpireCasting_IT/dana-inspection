﻿
namespace Dana_Inspection
{
    partial class WarningDialog
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AlertLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // AlertLbl
            // 
            this.AlertLbl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.AlertLbl.Font = new System.Drawing.Font("Century Schoolbook", 16F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.AlertLbl.Location = new System.Drawing.Point(0, 0);
            this.AlertLbl.Name = "AlertLbl";
            this.AlertLbl.Size = new System.Drawing.Size(429, 354);
            this.AlertLbl.TabIndex = 0;
            this.AlertLbl.Text = "Entry is out of range.\r\nPlease re-measure.";
            this.AlertLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // WarningDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Red;
            this.Controls.Add(this.AlertLbl);
            this.Name = "WarningDialog";
            this.Size = new System.Drawing.Size(429, 354);
            this.Load += new System.EventHandler(this.WarningDialog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label AlertLbl;
    }
}
