﻿
namespace Dana_Inspection
{
    partial class DataEntry
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DataEntry));
            this.GageTxtBox3 = new System.Windows.Forms.TextBox();
            this.GageTxtBox2 = new System.Windows.Forms.TextBox();
            this.GageTxtBox1 = new System.Windows.Forms.TextBox();
            this.BarCodeTxtBox = new System.Windows.Forms.TextBox();
            this.ExitBttn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.GageTxtBox5 = new System.Windows.Forms.TextBox();
            this.GageTxtBox4 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.FlagBttn = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.EditEntryBttn = new System.Windows.Forms.Button();
            this.UserLbl = new System.Windows.Forms.LinkLabel();
            this.TimeLbl = new System.Windows.Forms.Label();
            this.ClockTimer = new System.Windows.Forms.Timer(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.UserDocMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolTip = new System.Windows.Forms.ToolTip(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // GageTxtBox3
            // 
            this.GageTxtBox3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox3.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox3.Location = new System.Drawing.Point(600, 52);
            this.GageTxtBox3.Name = "GageTxtBox3";
            this.GageTxtBox3.Size = new System.Drawing.Size(100, 24);
            this.GageTxtBox3.TabIndex = 3;
            this.GageTxtBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GageTxtBox3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GageTxtBox3_KeyDown);
            // 
            // GageTxtBox2
            // 
            this.GageTxtBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox2.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox2.Location = new System.Drawing.Point(478, 52);
            this.GageTxtBox2.Name = "GageTxtBox2";
            this.GageTxtBox2.Size = new System.Drawing.Size(100, 24);
            this.GageTxtBox2.TabIndex = 2;
            this.GageTxtBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GageTxtBox2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GageTxtBox2_KeyDown);
            // 
            // GageTxtBox1
            // 
            this.GageTxtBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox1.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox1.Location = new System.Drawing.Point(356, 52);
            this.GageTxtBox1.Name = "GageTxtBox1";
            this.GageTxtBox1.Size = new System.Drawing.Size(100, 24);
            this.GageTxtBox1.TabIndex = 1;
            this.GageTxtBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GageTxtBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GageTxtBox1_KeyDown);
            // 
            // BarCodeTxtBox
            // 
            this.BarCodeTxtBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BarCodeTxtBox.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.BarCodeTxtBox.Location = new System.Drawing.Point(11, 52);
            this.BarCodeTxtBox.Name = "BarCodeTxtBox";
            this.BarCodeTxtBox.Size = new System.Drawing.Size(200, 24);
            this.BarCodeTxtBox.TabIndex = 0;
            this.BarCodeTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.BarCodeTxtBox.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BarCodeTxtBox_KeyDown);
            // 
            // ExitBttn
            // 
            this.ExitBttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ExitBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ExitBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ExitBttn.Location = new System.Drawing.Point(814, 408);
            this.ExitBttn.Name = "ExitBttn";
            this.ExitBttn.Size = new System.Drawing.Size(155, 61);
            this.ExitBttn.TabIndex = 8;
            this.ExitBttn.Text = "Exit";
            this.ExitBttn.UseVisualStyleBackColor = true;
            this.ExitBttn.Click += new System.EventHandler(this.ExitBttn_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.27366F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.78772F));
            this.tableLayoutPanel1.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanel1.Controls.Add(this.GageTxtBox5, 6, 1);
            this.tableLayoutPanel1.Controls.Add(this.GageTxtBox4, 5, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.BarCodeTxtBox, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.GageTxtBox1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.GageTxtBox2, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.GageTxtBox3, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel1.Controls.Add(this.FlagBttn, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label5, 4, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 93);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.Size = new System.Drawing.Size(960, 79);
            this.tableLayoutPanel1.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(850, 8);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(92, 32);
            this.label7.TabIndex = 13;
            this.label7.Text = "Datum R\r\n37.88 - 38.08";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GageTxtBox5
            // 
            this.GageTxtBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox5.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox5.Location = new System.Drawing.Point(844, 52);
            this.GageTxtBox5.Name = "GageTxtBox5";
            this.GageTxtBox5.Size = new System.Drawing.Size(104, 24);
            this.GageTxtBox5.TabIndex = 11;
            this.GageTxtBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GageTxtBox5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GageTxtBox5_KeyDown);
            // 
            // GageTxtBox4
            // 
            this.GageTxtBox4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox4.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox4.Location = new System.Drawing.Point(722, 52);
            this.GageTxtBox4.Name = "GageTxtBox4";
            this.GageTxtBox4.Size = new System.Drawing.Size(100, 24);
            this.GageTxtBox4.TabIndex = 11;
            this.GageTxtBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.GageTxtBox4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.GageTxtBox4_KeyDown);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(78, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(66, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Barcode";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(230, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 16);
            this.label2.TabIndex = 11;
            this.label2.Text = "Defective Part";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(352, 8);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(108, 32);
            this.label3.TabIndex = 11;
            this.label3.Text = "Tube Bore\r\n59.100 - 59.150\r\n";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(474, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(108, 48);
            this.label4.TabIndex = 11;
            this.label4.Text = "Pinion Oil Seal Bore\r\n80.16 - 80.26";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(714, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(116, 48);
            this.label6.TabIndex = 12;
            this.label6.Text = "Needle Bearing Clearance Bore\r\n46.958 - 46.996";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FlagBttn
            // 
            this.FlagBttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.FlagBttn.AutoSize = true;
            this.FlagBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.FlagBttn.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.FlagBttn.Location = new System.Drawing.Point(228, 51);
            this.FlagBttn.Name = "FlagBttn";
            this.FlagBttn.Size = new System.Drawing.Size(112, 27);
            this.FlagBttn.TabIndex = 11;
            this.FlagBttn.Text = "Flag Reason";
            this.FlagBttn.UseVisualStyleBackColor = true;
            this.FlagBttn.Click += new System.EventHandler(this.FlagBttn_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(592, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(116, 48);
            this.label5.TabIndex = 12;
            this.label5.Text = "Needle Bearing Bore\r\n69.770 - 69.820";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // EditEntryBttn
            // 
            this.EditEntryBttn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.EditEntryBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.EditEntryBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.EditEntryBttn.Location = new System.Drawing.Point(397, 408);
            this.EditEntryBttn.Name = "EditEntryBttn";
            this.EditEntryBttn.Size = new System.Drawing.Size(155, 61);
            this.EditEntryBttn.TabIndex = 10;
            this.EditEntryBttn.Text = "Edit Entry";
            this.EditEntryBttn.UseVisualStyleBackColor = true;
            this.EditEntryBttn.Click += new System.EventHandler(this.EditEntryBttn_Click);
            // 
            // UserLbl
            // 
            this.UserLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.UserLbl.AutoSize = true;
            this.UserLbl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.UserLbl.Location = new System.Drawing.Point(12, 429);
            this.UserLbl.Name = "UserLbl";
            this.UserLbl.Size = new System.Drawing.Size(55, 18);
            this.UserLbl.TabIndex = 11;
            this.UserLbl.TabStop = true;
            this.UserLbl.Text = "Login";
            this.UserLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.UserLbl.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.UserLbl_LinkClicked);
            // 
            // TimeLbl
            // 
            this.TimeLbl.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.TimeLbl.AutoSize = true;
            this.TimeLbl.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.TimeLbl.Location = new System.Drawing.Point(12, 451);
            this.TimeLbl.Name = "TimeLbl";
            this.TimeLbl.Size = new System.Drawing.Size(57, 18);
            this.TimeLbl.TabIndex = 12;
            this.TimeLbl.Text = "Time:";
            this.TimeLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ClockTimer
            // 
            this.ClockTimer.Enabled = true;
            this.ClockTimer.Interval = 1000;
            this.ClockTimer.Tick += new System.EventHandler(this.ClockTimer_Tick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.menuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.menuStrip1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(611, 9);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(101, 37);
            this.menuStrip1.TabIndex = 13;
            this.menuStrip1.Text = "Help";
            // 
            // helpToolStripMenuItem1
            // 
            this.helpToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.UserDocMenuItem,
            this.toolStripSeparator5,
            this.aboutMenuItem});
            this.helpToolStripMenuItem1.Font = new System.Drawing.Font("Arial", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.helpToolStripMenuItem1.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripMenuItem1.Image")));
            this.helpToolStripMenuItem1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.helpToolStripMenuItem1.Name = "helpToolStripMenuItem1";
            this.helpToolStripMenuItem1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.helpToolStripMenuItem1.Size = new System.Drawing.Size(93, 33);
            this.helpToolStripMenuItem1.Text = "&Help";
            // 
            // UserDocMenuItem
            // 
            this.UserDocMenuItem.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.UserDocMenuItem.Name = "UserDocMenuItem";
            this.UserDocMenuItem.Size = new System.Drawing.Size(237, 28);
            this.UserDocMenuItem.Text = "&Documentation";
            this.UserDocMenuItem.Click += new System.EventHandler(this.UserDocMenuItem_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(234, 6);
            // 
            // aboutMenuItem
            // 
            this.aboutMenuItem.Font = new System.Drawing.Font("Arial", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.aboutMenuItem.Name = "aboutMenuItem";
            this.aboutMenuItem.Size = new System.Drawing.Size(237, 28);
            this.aboutMenuItem.Text = "&About...";
            this.aboutMenuItem.Click += new System.EventHandler(this.aboutMenuItem_Click);
            // 
            // aboutToolTip
            // 
            this.aboutToolTip.AutoPopDelay = 15000;
            this.aboutToolTip.InitialDelay = 500;
            this.aboutToolTip.IsBalloon = true;
            this.aboutToolTip.ReshowDelay = 100;
            this.aboutToolTip.ShowAlways = true;
            this.aboutToolTip.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            this.aboutToolTip.ToolTipTitle = "Dana Inspection Application (DIA)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(12, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(223, 81);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 14;
            this.pictureBox1.TabStop = false;
            // 
            // DataEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(981, 481);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.TimeLbl);
            this.Controls.Add(this.UserLbl);
            this.Controls.Add(this.EditEntryBttn);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ExitBttn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "DataEntry";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Dana Data Entry";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.DataEntry_FormClosing);
            this.Load += new System.EventHandler(this.DataEntry_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox GageTxtBox3;
        private System.Windows.Forms.TextBox GageTxtBox2;
        private System.Windows.Forms.TextBox GageTxtBox1;
        private System.Windows.Forms.TextBox BarCodeTxtBox;
        private System.Windows.Forms.Button ExitBttn;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button EditEntryBttn;
        private System.Windows.Forms.TextBox GageTxtBox5;
        private System.Windows.Forms.TextBox GageTxtBox4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button FlagBttn;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.LinkLabel UserLbl;
        private System.Windows.Forms.Label TimeLbl;
        private System.Windows.Forms.Timer ClockTimer;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem UserDocMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem aboutMenuItem;
        private System.Windows.Forms.ToolTip aboutToolTip;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

