﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using de = Dana_Inspection.DataEntry;
using Repeater;

namespace Dana_Inspection
{
    public partial class DefectBttnUControl : UserControl
    {
        public DefectBttnUControl()
        {
            InitializeComponent();
        }

        private void FaultyBttnUControl_Load(object sender, EventArgs e)
        {

        }

        public void AddButton(FRButton b)
        {
            FaultyReasonBttn.Text = b.reason;
        }

        public void FaultyReasonBttn_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            if (btn.BackColor != System.Drawing.Color.SkyBlue)
            {
                btn.BackColor = System.Drawing.Color.SkyBlue;
                de.defects.Add(btn.Text);

                de.defectFlag = true;
            }
            else
            {
                btn.BackColor = SystemColors.ControlLight;
                de.defects.Remove(btn.Text);
            }
        }
    }
}
