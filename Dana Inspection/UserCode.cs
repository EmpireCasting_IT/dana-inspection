﻿using System;
using System.IO;
using gv = Dana_Inspection.GlobalVariables;
using gc = Dana_Inspection.GlobalOpClass;
using de = Dana_Inspection.DataEntry;
using System.Windows.Forms;
using System.Configuration;

namespace Dana_Inspection
{
    public partial class UserCode : Form
    {
        public static UserCode form;
        readonly EditDialog ed = new EditDialog();

        public string alphaCode = ConfigurationManager.AppSettings["AlphaCode"];
        string username;
        public static string[] passcode = { "" };
        public bool badBarcode;
        public bool badPart;
        public string MCodeFunction;

        private Control[] AllControls()
        {
            Control[] Controls = { Num0bttn, Num1bttn, Num2bttn, Num3bttn, Num4bttn, Num5bttn, Num6bttn, Num7bttn, Num8bttn, Num9bttn, PasscodeText, BackspaceBttn, SubmitBttn };

            return Controls;
        }

        private void HideControls()
        {
            foreach (Control control in AllControls())
            {
                control.Visible = false;
            }
        }

        private void ShowControls()
        {
            foreach (Control control in AllControls())
            {
                control.Visible = true;
            }
        }

        private void CodeFunction(string username)
        {
            bool manager = false;

            if (username != "Offline User")
            {
                try
                {
                    if (gc.SQLData("UPDATE UserCodes SET DateTime = '" + DateTime.Now + "' WHERE Passcode = @param1; SELECT Passcode FROM UserCodes WHERE Passcode = @param1 AND Authority = 'Administrator' OR Authority = 'Manager'", passcode).Rows[0][0].ToString() == passcode[0] || passcode[0] == alphaCode)
                    {
                        if (passcode[0] == alphaCode)
                        {
                            username = "Alpha User";
                        }

                        manager = true;
                    }
                    else
                    {
                        manager = false;
                    }
                }
                catch {}
            }
            else
            {
                if (passcode[0] == alphaCode)
                {
                    username = "Alpha User";
                    manager = true;
                }
                else
                {
                    manager = false;
                }
            }

            switch (MCodeFunction)
            {
                case "Login":
                    MCodeFunction = null;

                    gv.username = username;

                    de.form.inactiveTimeStamp = DateTime.Now.AddMilliseconds(gv.usageInterval);
                    // Use for Debugging
                    //MessageBox.Show("Logout: " + de.form.inactiveTimeStamp.ToString());

                    Close();
                    break;

                case "Edit":
                    MCodeFunction = null;

                    if (manager == true)
                    {
                        Close();
                        ed.ShowDialog();
                    }
                    else
                    {
                        Close();
                    }
                    break;

                case "Exit":
                    MCodeFunction = null;

                    if (manager == true)
                    {
                        Application.Exit();
                    }
                    else
                    {
                        Close();
                    }
                    break;

                default:
                    Close();
                    break;
            }
        }

        public UserCode()
        {
            form = this;
            InitializeComponent();
        }

        private void UserCode_Load(object sender, EventArgs e)
        {
            if (badBarcode == true)
            {
                WarningDialog wd = new WarningDialog("Invalid Barcode Entry:\n Please enter a valid barcode.");
                this.Controls.RemoveByKey("WarningDialog");
                this.Controls.Add(wd);
                this.Width = wd.Width;
                this.Height = wd.Height;

                badBarcode = false;
                this.Text = "";
                HideControls();
                CloseTimer.Start();
            }
            else if (badPart == true)
            {
                badPart = false;
                this.Text = "";
                WarningDialog1.Visible = true;
                HideControls();
                CloseTimer.Start();
            }
            else
            {
                if (MCodeFunction == "Login")
                {
                    this.Text = "User Code:";
                }
                else
                {
                    this.Text = "Manager Code:";
                }
                ActiveControl = PasscodeText;
                this.Controls.RemoveByKey("WarningDialog");
                WarningDialog1.Visible = false;
                this.Width = 501;
                this.Height = 628;
                ShowControls();
                CloseTimer.Stop();
            }

            ActiveControl = PasscodeText;
        }

        private void Num1bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 1;
        }

        private void Num2bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 2;
        }

        private void Num3bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 3;
        }

        private void Num4bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 4;
        }

        private void Num5bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 5;
        }

        private void Num6bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 6;
        }

        private void Num7bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 7;
        }

        private void Num8bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 8;
        }

        private void Num9bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 9;
        }

        private void Num0bttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text + 0;
        }

        private void BackspaceBttn_Click(object sender, EventArgs e)
        {
            PasscodeText.Text = PasscodeText.Text.Substring(0, PasscodeText.Text.Length - 1);
        }

        private void SubmitBttn_Click(object sender, EventArgs e)
        {
            try
            {
                passcode[0] = PasscodeText.Text.Trim();
                username = gc.SQLString("SELECT Username FROM UserCodes WHERE Passcode = @param1", passcode);

                if (username != null)
                {
                    CodeFunction(username);
                }
                else
                {
                    throw new Exception("Username assignment error.");
                }
            }
            catch (Exception ex)
            {
                if (ex.Message.Contains("Length cannot be"))
                {
                    gv.username = "Login";
                    PasscodeText.Clear();
                }

                if (gc.CheckForInternetConnection() == false)
                {
                    if (! File.Exists(gv.backupFile) && passcode[0] != alphaCode)
                    {
                        MessageBox.Show("There is no internet connection.\n\nPlease use Alpha Passcode.", "Internet Connectivity Error:");
                    }

                    CodeFunction("Offline User");
                }
            }

            // Always Clear Passcode
            PasscodeText.Clear();
        }

        private void closeTimer_Tick(object sender, EventArgs e)
        {
            Visible = false;

            CloseTimer.Stop();
        }

        private void UserCode_FormClosing(object sender, FormClosingEventArgs e)
        {
            MCodeFunction = null;
        }
    }
}
