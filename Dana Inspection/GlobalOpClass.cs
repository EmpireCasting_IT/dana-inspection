﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System.Reflection;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using gv = Dana_Inspection.GlobalVariables;
using de = Dana_Inspection.DataEntry;

namespace Dana_Inspection
{
    public class GlobalOpClass
    {
        public static readonly string AppName = Assembly.GetExecutingAssembly().GetName().Name;
        public static string AppEnvironment = ConfigurationManager.AppSettings["Mode"];
        public static string AppLocation = Environment.MachineName.ToString() + ": " + AppDomain.CurrentDomain.BaseDirectory;
        public static readonly string connString = ConfigurationManager.ConnectionStrings["ConnString"].ConnectionString;

        public static void myEnvironment()
        {
            // Change environment to Testing if using Test Account
            if (gv.username == "Test\n")
            {
                gv.environment = "Test_";
                AppEnvironment = "Testing";
                AppLocation = Environment.MachineName.ToString() + ": " + AppDomain.CurrentDomain.BaseDirectory.Substring(AppDomain.CurrentDomain.BaseDirectory.LastIndexOf(AppName));
            }
            else
            {
                if (!ConfigurationManager.AppSettings["Mode"].Contains("Test"))
                {
                    // Clear global Deployment Environment variable if in Production
                    gv.environment = "";

                    AppEnvironment = "Production";
                }
                else
                {
                    AppEnvironment = "Testing";
                    AppLocation = Environment.MachineName.ToString() + ": " + AppDomain.CurrentDomain.BaseDirectory.Substring(AppDomain.CurrentDomain.BaseDirectory.LastIndexOf(AppName));
                }
            }
        }

        public static string SQLString(string SQL, [Optional] string[] parameters)
        {
            try
            {
                if (CheckForInternetConnection() == true)
                {
                    // Assign environment variables
                    myEnvironment();

                    int count = 1;

                    using (SqlConnection conn = new SqlConnection())
                    {
                        SqlDataReader dataReader;
                        string Output = "";

                        conn.ConnectionString = connString;
                        conn.Open();

                        SqlCommand command = new SqlCommand(SQL, conn);
                        if (parameters != null)
                        {
                            foreach (string param in parameters)
                            {
                                if (param != "")
                                {
                                    command.Parameters.Add(new SqlParameter("param" + count, param.Trim()));
                                }
                                else
                                {
                                    command.Parameters.Add(new SqlParameter("param" + count, DBNull.Value));
                                }

                                count += 1;
                            }
                        }

                        if (SQL.Contains("SELECT"))
                        {
                            dataReader = command.ExecuteReader();

                            while (dataReader.Read())
                            {
                                for (int i = 0; i < dataReader.FieldCount; i++)
                                {
                                    Output = Output + dataReader.GetValue(i) + ", ";
                                }
                            }
                            Output = Output.Substring(0, Output.Length - 2);
                            Output += "\n";

                            // Print Output to MessageBox for Testing Purposes
                            //de.form.msg(Output);

                            return Output;
                        }
                        else
                        {
                            command.ExecuteNonQuery();
                        }
                    }
                }
                else
                {
                    Backup(SQL, parameters, "Inspection");
                }
            }
            catch (SqlException err)
            {
                de.form.msg(err.Message, "SQL Server error:");

                if (! err.Message.Contains("network-related"))
                {
                    DataTable valRange = SQLData("SELECT StartRange, EndRange FROM InspectValidation");

                    // Log Error to Database
                    if (parameters[0].Contains(valRange.Rows[0][0].ToString()))
                    {
                        ErrorLogger("DB", AppName, "BC: " + parameters[0] + " - " + err.Message, err.StackTrace);
                    }
                    else
                    {
                        ErrorLogger("DB", AppName, err.Message, err.StackTrace);
                    }
                }
                else
                {
                    string[] valStart = ConfigurationManager.AppSettings["ValidateStart"].Split(", ");

                    // Log Error to File
                    if (parameters[0].Contains(valStart[0].ToString()))
                    {
                        ErrorLogger("File", AppName, "BC: " + parameters[0] + " - " + err.Message, err.StackTrace);
                    }
                    else
                    {
                        ErrorLogger("File", AppName, err.Message, err.StackTrace);
                    }

                    Backup(SQL, parameters, "Inspection");
                }
            }

            return null;
        }

        public static DataTable SQLData(string SQL, [Optional] string[] parameters)
        {
            try
            {
                if (CheckForInternetConnection() == true)
                {
                    // Assign environment variables
                    myEnvironment();

                    int count = 1;

                    using (SqlConnection conn = new SqlConnection())
                    {
                        conn.ConnectionString = connString;
                        conn.Open();

                        SqlCommand command = new SqlCommand(SQL, conn);
                        if (parameters != null)
                        {
                            foreach (string param in parameters)
                            {
                                if (param != "")
                                {
                                    command.Parameters.Add(new SqlParameter("param" + count, param.Trim()));
                                }
                                else
                                {
                                    command.Parameters.Add(new SqlParameter("param" + count, DBNull.Value));
                                }

                                count += 1;
                            }
                        }

                        SqlDataAdapter da = new SqlDataAdapter();
                        DataTable dt = new DataTable();

                        da.SelectCommand = command;
                        da.Fill(dt);

                        if (SQL.Contains("SELECT") || SQL.Contains("EXEC"))
                        {
                            return dt;
                        }
                        else if (SQL.Contains("INSERT"))
                        {

                        }
                        else if (SQL.Contains("UPDATE"))
                        {
                            da.Update(dt);
                        }
                        else if (SQL.Contains("DELETE"))
                        {

                        }
                    }
                }
            }
            catch (SqlException err)
            {
                de.form.msg(err.Message, "SQL Server error:");

                if (! err.Message.Contains("network-related"))
                {
                    // Log Error to Database
                    ErrorLogger("DB", AppName, err.Message, err.StackTrace);
                }
                else
                {
                    // Log Error to File
                    ErrorLogger("File", AppName, err.Message, err.StackTrace);
                }
            }

            return null;
        }

        private static void Backup(string SQL, string[] parameters, string tableName)
        {
            try
            {
                string backupDir = ConfigurationManager.AppSettings["DB_Backup"];
                string fileShareDir = ConfigurationManager.AppSettings["DB_FileShare"];
                string fileName = backupDir + tableName + "_Data.csv";
                bool fileExists = false;
                string fieldNames = ConfigurationManager.AppSettings["Db_" + tableName + "_Columns"].ToString();
                gv.backupFile = fileName;

                switch (tableName)
                {
                    default:
                        // Code for CSV file Entry
                        if (SQL.Contains("INSERT INTO " + tableName))
                        {
                            // Check if Backup Directory exists
                            if (!Directory.Exists(backupDir))
                            {
                                Directory.CreateDirectory(backupDir);
                            }
                            // Check if Backup File exists
                            if (File.Exists(fileName))
                            {
                                fileExists = true;
                            }

                            StreamWriter sw2 = new StreamWriter(fileName, true);

                            // Write column names to CSV header
                            if (fileExists == false)
                            {
                                sw2.Write(fieldNames);
                                sw2.Write("\r\n");
                            }

                            for (int i = 0; i < parameters.Count() - 1; i++)
                            {
                                {
                                    sw2.Write("\"" + parameters[i] + "\"");
                                    sw2.Write(", ");
                                }
                            }
                            sw2.Write(DateTime.Now);
                            sw2.Write("\r\n");
                            sw2.Flush();
                            sw2.Close();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                de.form.msg(ex.Message, "Backup Error:");
                ErrorLogger("File", AppName, ex.Message, ex.StackTrace);
            }
        }

        public static void UploadBackup(string CSVpath, string tableName)
        {
            const string CSV_ConnString = "Provider=Microsoft.ACE.OLEDB.12.0 ;Data Source=\"{0}\";Extended Properties=\"text;HDR=YES;FMT=Delimited\"";
            var AllFiles = new DirectoryInfo(CSVpath).GetFiles("*.csv");
            string ConStr = connString;
            string backupDir = ConfigurationManager.AppSettings["DB_Backup"];
            string fileTimeStamp = DateTime.Now.ToString("MM-dd-yyyy hh.mm.ss tt");


            // Loop through every file in CSV root directory
            for (int i = 0; i < AllFiles.Length; i++)
            {
                try
                {
                    // Check if Archive Directory exists
                    if (!Directory.Exists(backupDir + "Archive"))
                    {
                        Directory.CreateDirectory(backupDir + "Archive");
                    }

                    string File_Name = AllFiles[i].Name;
                    DataTable dt = new DataTable();
                    using (OleDbConnection con = new OleDbConnection(string.Format(CSV_ConnString, CSVpath)))
                    {
                        using (OleDbDataAdapter da = new OleDbDataAdapter("SELECT * FROM [" + File_Name + "]", con))
                        {
                            da.Fill(dt);
                        }
                    }
                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(ConStr))
                    {
                        DataTable tableNames = SQLData("SELECT column_name FROM information_schema.columns WHERE table_name='" + tableName + "'");

                        for (i = 0; i < tableNames.Rows.Count; i++)
                        {
                            if (tableNames.Rows[i][0].ToString() != "Id")
                            {
                                bulkCopy.ColumnMappings.Add(i - 1, tableNames.Rows[i][0].ToString());
                            }
                        }
                        bulkCopy.DestinationTableName = tableName;
                        bulkCopy.BatchSize = dt.Rows.Count;
                        bulkCopy.WriteToServer(dt);
                        bulkCopy.Close();
                    }

                    string archiveFile = File_Name.Substring(0, File_Name.IndexOf(".csv")) + " (" + fileTimeStamp + ").csv";

                    File.Move(CSVpath + File_Name, backupDir + "Archive\\" + archiveFile);
                }
                catch (Exception err)
                {
                    de.form.msg(err.Message, "Upload Backup Error:");
                    //ErrorLogger("DB", AppName, err.Message, err.StackTrace);
                }
            }
        }

        public static void ErrorLogger(string logType, string appName, string errMessage, string stackTrace)
        {
            string fileTimeStamp = DateTime.Now.ToString("MM-dd-yyyy");

            switch (logType)
            {
                case "DB":
                    string[] dataParams = { appName, AppEnvironment , AppLocation, errMessage, stackTrace.Substring(stackTrace.LastIndexOf("\\") + 1), DateTime.Now.ToString() };

                    try
                    {
                        SQLData("INSERT INTO ErrorLogs (AppName, Environment, Location, ErrorMessage, StackTrace, DateTime) VALUES (@param1, @param2, @param3, @param4, @param5, @param6)", dataParams);
                    }
                    catch (Exception ex)
                    {
                        if (! Directory.Exists(ConfigurationManager.AppSettings["ErrorFile"]))
                        {
                            Directory.CreateDirectory(ConfigurationManager.AppSettings["ErrorFile"]);
                        }

                        StreamWriter sw = new StreamWriter(ConfigurationManager.AppSettings["ErrorFile"] + "Log (" + fileTimeStamp + ").txt", true);
                        sw.Write("App Name: " + AppName + "\n" +
                                 "Location: " + AppLocation + "\n" +
                                 "Date: " + DateTime.Now + "\n" +
                                 "Error: " + ex.Message + "\n" +
                                 "StackTrace: " + stackTrace + "\n");
                        sw.Write("\r\n");
                        sw.Flush();
                        sw.Close();
                    }

                    break;

                case "File":
                    if (!Directory.Exists(ConfigurationManager.AppSettings["ErrorFile"]))
                    {
                        Directory.CreateDirectory(ConfigurationManager.AppSettings["ErrorFile"]);
                    }

                    StreamWriter sw2 = new StreamWriter(ConfigurationManager.AppSettings["ErrorFile"] + "Log (" + fileTimeStamp + ").txt", true);
                    sw2.Write("App Name: " + AppName + "\n" +
                              "Location: " + AppLocation + "\n" +
                              "Date: " + DateTime.Now + "\n" +
                              "Error: " + errMessage + "\n" +
                              "StackTrace: " + stackTrace + "\n");
                    sw2.Write("\r\n");
                    sw2.Flush();
                    sw2.Close();

                    break;
            }
        }

        public static string ConvertToJulian(DateTime now)
        {
            // Julian Day by Month
            int[] dayLookUp = { 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334 };            

            int Month = now.Month;
            int Day = now.Day;
            int julDay = dayLookUp[Month - 1] + Day;
            int Year = now.Year;

            bool leapYear = DateTime.IsLeapYear(Year);
            int nDate;

            // Leap Year Switch
            switch (leapYear)
            {
                case true:
                    julDay =+ 1;
                    nDate = (Year * 1000) + julDay;
                    
                    break;

                default:
                    nDate = (Year * 1000) + julDay;

                    break;
            }

            
            return nDate.ToString();
        }

        public static string ConvertTime(DateTime now)
        {
            string nTime = now.ToString("HHmmss");


            return nTime;
        }

        public static bool CheckForInternetConnection(int timeoutMs = 10000, string url = null)
        {
            try
            {
                url ??= CultureInfo.InstalledUICulture switch
                {
                    { Name: var n } when n.StartsWith("fa") => // Iran
                        "http://www.aparat.com",
                    { Name: var n } when n.StartsWith("zh") => // China
                        "http://www.baidu.com",
                    _ =>
                        "http://www.gstatic.com/generate_204",
                };

                var request = (HttpWebRequest)WebRequest.Create(url);
                request.KeepAlive = false;
                request.Timeout = timeoutMs;
                using var response = (HttpWebResponse)request.GetResponse();
                return true;
            }
            catch
            {
                ErrorLogger("File", AppName, "No Internet Connection", "Please diagnose internet connectivity.");

                return false;
            }
        }
    }
}

namespace Repeater
{
    public class FRButton
    {
        public string reason { get; set; }

    }
}