﻿
namespace Dana_Inspection
{
    partial class UserCode
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.WarningDialog1 = new Dana_Inspection.WarningDialog();
            this.CloseTimer = new System.Windows.Forms.Timer(this.components);
            this.GageTxtBox5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SubmitBttn = new System.Windows.Forms.Button();
            this.Num0bttn = new System.Windows.Forms.Button();
            this.Num9bttn = new System.Windows.Forms.Button();
            this.Num8bttn = new System.Windows.Forms.Button();
            this.Num7bttn = new System.Windows.Forms.Button();
            this.Num6bttn = new System.Windows.Forms.Button();
            this.Num5bttn = new System.Windows.Forms.Button();
            this.Num4bttn = new System.Windows.Forms.Button();
            this.Num3bttn = new System.Windows.Forms.Button();
            this.Num2bttn = new System.Windows.Forms.Button();
            this.Num1bttn = new System.Windows.Forms.Button();
            this.PasscodeText = new System.Windows.Forms.TextBox();
            this.BackspaceBttn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // WarningDialog1
            // 
            this.WarningDialog1.BackColor = System.Drawing.Color.Red;
            this.WarningDialog1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.WarningDialog1.Location = new System.Drawing.Point(0, 0);
            this.WarningDialog1.Name = "WarningDialog1";
            this.WarningDialog1.Size = new System.Drawing.Size(485, 589);
            this.WarningDialog1.TabIndex = 0;
            this.WarningDialog1.Visible = false;
            // 
            // CloseTimer
            // 
            this.CloseTimer.Enabled = true;
            this.CloseTimer.Interval = 4000;
            this.CloseTimer.Tick += new System.EventHandler(this.closeTimer_Tick);
            // 
            // GageTxtBox5
            // 
            this.GageTxtBox5.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.GageTxtBox5.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.GageTxtBox5.Location = new System.Drawing.Point(723, 43);
            this.GageTxtBox5.Name = "GageTxtBox5";
            this.GageTxtBox5.Size = new System.Drawing.Size(120, 24);
            this.GageTxtBox5.TabIndex = 11;
            this.GageTxtBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(171, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 100);
            this.label7.TabIndex = 13;
            this.label7.Text = "Datum R\r\n37.88 - 38.08";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.tableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.tableLayoutPanel1.ColumnCount = 7;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.28571F));
            this.tableLayoutPanel1.Controls.Add(this.label7, 6, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(200, 100);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // SubmitBttn
            // 
            this.SubmitBttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SubmitBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SubmitBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SubmitBttn.Location = new System.Drawing.Point(313, 471);
            this.SubmitBttn.Name = "SubmitBttn";
            this.SubmitBttn.Size = new System.Drawing.Size(132, 33);
            this.SubmitBttn.TabIndex = 42;
            this.SubmitBttn.Text = "Submit";
            this.SubmitBttn.UseVisualStyleBackColor = true;
            this.SubmitBttn.Click += new System.EventHandler(this.SubmitBttn_Click);
            // 
            // Num0bttn
            // 
            this.Num0bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num0bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num0bttn.Location = new System.Drawing.Point(40, 403);
            this.Num0bttn.Name = "Num0bttn";
            this.Num0bttn.Size = new System.Drawing.Size(130, 101);
            this.Num0bttn.TabIndex = 40;
            this.Num0bttn.Text = "0";
            this.Num0bttn.UseVisualStyleBackColor = true;
            this.Num0bttn.Click += new System.EventHandler(this.Num0bttn_Click);
            // 
            // Num9bttn
            // 
            this.Num9bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num9bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num9bttn.Location = new System.Drawing.Point(313, 297);
            this.Num9bttn.Name = "Num9bttn";
            this.Num9bttn.Size = new System.Drawing.Size(132, 100);
            this.Num9bttn.TabIndex = 39;
            this.Num9bttn.Text = "9";
            this.Num9bttn.UseVisualStyleBackColor = true;
            this.Num9bttn.Click += new System.EventHandler(this.Num9bttn_Click);
            // 
            // Num8bttn
            // 
            this.Num8bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num8bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num8bttn.Location = new System.Drawing.Point(176, 297);
            this.Num8bttn.Name = "Num8bttn";
            this.Num8bttn.Size = new System.Drawing.Size(131, 100);
            this.Num8bttn.TabIndex = 38;
            this.Num8bttn.Text = "8";
            this.Num8bttn.UseVisualStyleBackColor = true;
            this.Num8bttn.Click += new System.EventHandler(this.Num8bttn_Click);
            // 
            // Num7bttn
            // 
            this.Num7bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num7bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num7bttn.Location = new System.Drawing.Point(40, 297);
            this.Num7bttn.Name = "Num7bttn";
            this.Num7bttn.Size = new System.Drawing.Size(130, 100);
            this.Num7bttn.TabIndex = 37;
            this.Num7bttn.Text = "7";
            this.Num7bttn.UseVisualStyleBackColor = true;
            this.Num7bttn.Click += new System.EventHandler(this.Num7bttn_Click);
            // 
            // Num6bttn
            // 
            this.Num6bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num6bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num6bttn.Location = new System.Drawing.Point(313, 191);
            this.Num6bttn.Name = "Num6bttn";
            this.Num6bttn.Size = new System.Drawing.Size(132, 100);
            this.Num6bttn.TabIndex = 36;
            this.Num6bttn.Text = "6";
            this.Num6bttn.UseVisualStyleBackColor = true;
            this.Num6bttn.Click += new System.EventHandler(this.Num6bttn_Click);
            // 
            // Num5bttn
            // 
            this.Num5bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num5bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num5bttn.Location = new System.Drawing.Point(176, 191);
            this.Num5bttn.Name = "Num5bttn";
            this.Num5bttn.Size = new System.Drawing.Size(131, 100);
            this.Num5bttn.TabIndex = 35;
            this.Num5bttn.Text = "5";
            this.Num5bttn.UseVisualStyleBackColor = true;
            this.Num5bttn.Click += new System.EventHandler(this.Num5bttn_Click);
            // 
            // Num4bttn
            // 
            this.Num4bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num4bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num4bttn.Location = new System.Drawing.Point(40, 191);
            this.Num4bttn.Name = "Num4bttn";
            this.Num4bttn.Size = new System.Drawing.Size(130, 100);
            this.Num4bttn.TabIndex = 34;
            this.Num4bttn.Text = "4";
            this.Num4bttn.UseVisualStyleBackColor = true;
            this.Num4bttn.Click += new System.EventHandler(this.Num4bttn_Click);
            // 
            // Num3bttn
            // 
            this.Num3bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num3bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num3bttn.Location = new System.Drawing.Point(313, 85);
            this.Num3bttn.Name = "Num3bttn";
            this.Num3bttn.Size = new System.Drawing.Size(132, 100);
            this.Num3bttn.TabIndex = 33;
            this.Num3bttn.Text = "3";
            this.Num3bttn.UseVisualStyleBackColor = true;
            this.Num3bttn.Click += new System.EventHandler(this.Num3bttn_Click);
            // 
            // Num2bttn
            // 
            this.Num2bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num2bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num2bttn.Location = new System.Drawing.Point(176, 85);
            this.Num2bttn.Name = "Num2bttn";
            this.Num2bttn.Size = new System.Drawing.Size(131, 100);
            this.Num2bttn.TabIndex = 32;
            this.Num2bttn.Text = "2";
            this.Num2bttn.UseVisualStyleBackColor = true;
            this.Num2bttn.Click += new System.EventHandler(this.Num2bttn_Click);
            // 
            // Num1bttn
            // 
            this.Num1bttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.Num1bttn.Font = new System.Drawing.Font("Verdana", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Num1bttn.Location = new System.Drawing.Point(40, 85);
            this.Num1bttn.Name = "Num1bttn";
            this.Num1bttn.Size = new System.Drawing.Size(130, 100);
            this.Num1bttn.TabIndex = 31;
            this.Num1bttn.Text = "1";
            this.Num1bttn.UseVisualStyleBackColor = true;
            this.Num1bttn.Click += new System.EventHandler(this.Num1bttn_Click);
            // 
            // PasscodeText
            // 
            this.PasscodeText.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PasscodeText.Font = new System.Drawing.Font("Verdana", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.PasscodeText.Location = new System.Drawing.Point(176, 434);
            this.PasscodeText.Multiline = true;
            this.PasscodeText.Name = "PasscodeText";
            this.PasscodeText.PasswordChar = '*';
            this.PasscodeText.PlaceholderText = "Passcode";
            this.PasscodeText.Size = new System.Drawing.Size(131, 28);
            this.PasscodeText.TabIndex = 41;
            this.PasscodeText.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // BackspaceBttn
            // 
            this.BackspaceBttn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.BackspaceBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackspaceBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BackspaceBttn.Location = new System.Drawing.Point(313, 434);
            this.BackspaceBttn.Name = "BackspaceBttn";
            this.BackspaceBttn.Size = new System.Drawing.Size(132, 33);
            this.BackspaceBttn.TabIndex = 43;
            this.BackspaceBttn.Text = "<";
            this.BackspaceBttn.UseVisualStyleBackColor = true;
            this.BackspaceBttn.Click += new System.EventHandler(this.BackspaceBttn_Click);
            // 
            // UserCode
            // 
            this.AcceptButton = this.SubmitBttn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(485, 589);
            this.Controls.Add(this.BackspaceBttn);
            this.Controls.Add(this.SubmitBttn);
            this.Controls.Add(this.Num0bttn);
            this.Controls.Add(this.Num9bttn);
            this.Controls.Add(this.Num8bttn);
            this.Controls.Add(this.Num7bttn);
            this.Controls.Add(this.Num6bttn);
            this.Controls.Add(this.Num5bttn);
            this.Controls.Add(this.Num4bttn);
            this.Controls.Add(this.Num3bttn);
            this.Controls.Add(this.Num2bttn);
            this.Controls.Add(this.Num1bttn);
            this.Controls.Add(this.PasscodeText);
            this.Controls.Add(this.WarningDialog1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserCode";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "User Code:";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserCode_FormClosing);
            this.Load += new System.EventHandler(this.UserCode_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WarningDialog WarningDialog1;
        private System.Windows.Forms.Timer CloseTimer;
        private System.Windows.Forms.TextBox GageTxtBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button BackspaceBttn;
        private System.Windows.Forms.Button SubmitBttn;
        private System.Windows.Forms.Button Num0bttn;
        private System.Windows.Forms.Button Num9bttn;
        private System.Windows.Forms.Button Num8bttn;
        private System.Windows.Forms.Button Num7bttn;
        private System.Windows.Forms.Button Num6bttn;
        private System.Windows.Forms.Button Num5bttn;
        private System.Windows.Forms.Button Num4bttn;
        private System.Windows.Forms.Button Num3bttn;
        private System.Windows.Forms.Button Num2bttn;
        private System.Windows.Forms.Button Num1bttn;
        private System.Windows.Forms.TextBox PasscodeText;
    }
}