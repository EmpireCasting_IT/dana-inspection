﻿
namespace Dana_Inspection
{
    partial class Defect_Reasons
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Defect_Reasons));
            this.OKBttn = new System.Windows.Forms.Button();
            this.FPartPanel = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // OKBttn
            // 
            this.OKBttn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.OKBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.OKBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.OKBttn.Location = new System.Drawing.Point(633, 267);
            this.OKBttn.Name = "OKBttn";
            this.OKBttn.Size = new System.Drawing.Size(155, 61);
            this.OKBttn.TabIndex = 9;
            this.OKBttn.Text = "OK";
            this.OKBttn.UseVisualStyleBackColor = true;
            this.OKBttn.Click += new System.EventHandler(this.OKBttn_Click);
            // 
            // FPartPanel
            // 
            this.FPartPanel.AutoScroll = true;
            this.FPartPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FPartPanel.Location = new System.Drawing.Point(0, 0);
            this.FPartPanel.Name = "FPartPanel";
            this.FPartPanel.Size = new System.Drawing.Size(800, 340);
            this.FPartPanel.TabIndex = 10;
            // 
            // Defect_Reasons
            // 
            this.AcceptButton = this.OKBttn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 340);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.FPartPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Defect_Reasons";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Defect Reasons:";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Faulty_Reasons_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Panel FPartPanel;
    }
}