﻿
namespace Dana_Inspection
{
    partial class EditDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EditDialog));
            this.EditDataGrid = new System.Windows.Forms.DataGridView();
            this.DefReasonLbl = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ClearBttn = new System.Windows.Forms.Button();
            this.DefectLbl = new System.Windows.Forms.Label();
            this.SaveBttn = new System.Windows.Forms.Button();
            this.DefectCBox = new System.Windows.Forms.CheckBox();
            this.ReasonComboBox = new System.Windows.Forms.ComboBox();
            this.BarcodeLbl = new System.Windows.Forms.Label();
            this.BarcodeTxtBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.EditDataGrid)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // EditDataGrid
            // 
            this.EditDataGrid.AllowUserToAddRows = false;
            this.EditDataGrid.AllowUserToDeleteRows = false;
            this.EditDataGrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.EditDataGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.EditDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.EditDataGrid.Location = new System.Drawing.Point(2, 0);
            this.EditDataGrid.Margin = new System.Windows.Forms.Padding(4);
            this.EditDataGrid.Name = "EditDataGrid";
            this.EditDataGrid.ReadOnly = true;
            this.EditDataGrid.RowTemplate.Height = 25;
            this.EditDataGrid.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EditDataGrid.Size = new System.Drawing.Size(804, 578);
            this.EditDataGrid.TabIndex = 0;
            this.EditDataGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.EditDataGrid_CellClick);
            // 
            // DefReasonLbl
            // 
            this.DefReasonLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DefReasonLbl.AutoSize = true;
            this.DefReasonLbl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.DefReasonLbl.Location = new System.Drawing.Point(64, 154);
            this.DefReasonLbl.Name = "DefReasonLbl";
            this.DefReasonLbl.Size = new System.Drawing.Size(69, 21);
            this.DefReasonLbl.TabIndex = 3;
            this.DefReasonLbl.Text = "Reason:";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ClearBttn, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.DefectLbl, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.SaveBttn, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.DefectCBox, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.ReasonComboBox, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.DefReasonLbl, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.BarcodeLbl, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.BarcodeTxtBox, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(813, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 192F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(197, 557);
            this.tableLayoutPanel1.TabIndex = 7;
            // 
            // ClearBttn
            // 
            this.ClearBttn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ClearBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClearBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.ClearBttn.Location = new System.Drawing.Point(21, 422);
            this.ClearBttn.Name = "ClearBttn";
            this.ClearBttn.Size = new System.Drawing.Size(155, 61);
            this.ClearBttn.TabIndex = 4;
            this.ClearBttn.Text = "Clear";
            this.ClearBttn.UseVisualStyleBackColor = true;
            this.ClearBttn.Click += new System.EventHandler(this.ClearBttn_Click);
            // 
            // DefectLbl
            // 
            this.DefectLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.DefectLbl.AutoSize = true;
            this.DefectLbl.Font = new System.Drawing.Font("Segoe UI", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point);
            this.DefectLbl.Location = new System.Drawing.Point(53, 82);
            this.DefectLbl.Name = "DefectLbl";
            this.DefectLbl.Size = new System.Drawing.Size(90, 21);
            this.DefectLbl.TabIndex = 0;
            this.DefectLbl.Text = "Defective?";
            // 
            // SaveBttn
            // 
            this.SaveBttn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.SaveBttn.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.SaveBttn.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.SaveBttn.Location = new System.Drawing.Point(21, 493);
            this.SaveBttn.Name = "SaveBttn";
            this.SaveBttn.Size = new System.Drawing.Size(155, 61);
            this.SaveBttn.TabIndex = 5;
            this.SaveBttn.Text = "Save";
            this.SaveBttn.UseVisualStyleBackColor = true;
            this.SaveBttn.Click += new System.EventHandler(this.SaveBttn_Click);
            // 
            // DefectCBox
            // 
            this.DefectCBox.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.DefectCBox.AutoSize = true;
            this.DefectCBox.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.DefectCBox.Location = new System.Drawing.Point(56, 113);
            this.DefectCBox.Name = "DefectCBox";
            this.DefectCBox.Size = new System.Drawing.Size(84, 25);
            this.DefectCBox.TabIndex = 2;
            this.DefectCBox.Text = "Yes/No";
            this.DefectCBox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.DefectCBox.UseVisualStyleBackColor = true;
            this.DefectCBox.CheckedChanged += new System.EventHandler(this.DefectCBox_CheckedChanged);
            // 
            // ReasonComboBox
            // 
            this.ReasonComboBox.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.ReasonComboBox.FormattingEnabled = true;
            this.ReasonComboBox.Location = new System.Drawing.Point(38, 178);
            this.ReasonComboBox.Name = "ReasonComboBox";
            this.ReasonComboBox.Size = new System.Drawing.Size(121, 29);
            this.ReasonComboBox.TabIndex = 3;
            // 
            // BarcodeLbl
            // 
            this.BarcodeLbl.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.BarcodeLbl.AutoSize = true;
            this.BarcodeLbl.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BarcodeLbl.Location = new System.Drawing.Point(60, 8);
            this.BarcodeLbl.Name = "BarcodeLbl";
            this.BarcodeLbl.Size = new System.Drawing.Size(76, 21);
            this.BarcodeLbl.TabIndex = 6;
            this.BarcodeLbl.Text = "Barcode:";
            // 
            // BarcodeTxtBox
            // 
            this.BarcodeTxtBox.Dock = System.Windows.Forms.DockStyle.Top;
            this.BarcodeTxtBox.Location = new System.Drawing.Point(3, 32);
            this.BarcodeTxtBox.Name = "BarcodeTxtBox";
            this.BarcodeTxtBox.PlaceholderText = "Search";
            this.BarcodeTxtBox.Size = new System.Drawing.Size(191, 29);
            this.BarcodeTxtBox.TabIndex = 1;
            this.BarcodeTxtBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.BarcodeTxtBox.TextChanged += new System.EventHandler(this.BarcodeTxtBox_TextChanged);
            // 
            // EditDialog
            // 
            this.AcceptButton = this.SaveBttn;
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 21F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1013, 581);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.EditDataGrid);
            this.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "EditDialog";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edit Dialog";
            this.TopMost = true;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.EditDialog_Load);
            ((System.ComponentModel.ISupportInitialize)(this.EditDataGrid)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView EditDataGrid;
        private System.Windows.Forms.Label DefReasonLbl;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label BarcodeLbl;
        private System.Windows.Forms.Label DefectLbl;
        private System.Windows.Forms.TextBox BarcodeTxtBox;
        private System.Windows.Forms.ComboBox ReasonComboBox;
        private System.Windows.Forms.Button SaveBttn;
        private System.Windows.Forms.CheckBox DefectCBox;
        private System.Windows.Forms.Button ClearBttn;
    }
}