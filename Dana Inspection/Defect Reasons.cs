﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using Repeater;
using gc = Dana_Inspection.GlobalOpClass;
using de = Dana_Inspection.DataEntry;
using System.Windows.Forms;
using System.Linq;

namespace Dana_Inspection
{
    public partial class Defect_Reasons : Form
    {
        public static Defect_Reasons form;

        List<FRButton> fRButtons = new List<FRButton>();

        public Defect_Reasons()
        {
            form = this;
            InitializeComponent();

            InitDB();

            AddButtons(fRButtons);
        }

        private void Faulty_Reasons_Load(object sender, EventArgs e)
        {
            
        }

        private void InitDB()
        {
            try
            {
                for (int i = 0; i < gc.SQLData("SELECT Reason FROM BreakageTypes").Rows.Count; i++)
                {
                    FRButton b = new FRButton
                    {
                        reason = gc.SQLData("SELECT Reason FROM BreakageTypes").Rows[i][0].ToString()
                    };
                    fRButtons.Add(b);
                }
            }
            catch { }
        }

        private void AddButtons(List<FRButton> list)
        {
            int x = 0;
            int y = 0;
            int i = 0;

            foreach (FRButton r in list)
            {
                DefectBttnUControl u = new DefectBttnUControl();

                u.BackColor = System.Drawing.Color.Khaki;
                u.Location = new System.Drawing.Point(-1 + x, 1);
                u.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)));
                u.Name = "userControl_" + r;
                u.Size = new System.Drawing.Size(150, 100);
                u.TabIndex = i++;
                u.Click += u.FaultyReasonBttn_Click;
                u.AddButton(r);
                FPartPanel.Controls.Add(u);
                x += u.Width;
                if (y % 6 == 0)
                {
                    y += u.Height;
                }
            }
            FPartPanel.Height = y;
            FPartPanel.Width = x;
            this.Height = y + 300;
            this.Width = x + 20;
        }

        private void OKBttn_Click(object sender, EventArgs e)
        {
            if (de.form.TextBoxes()[0].Text != "" || de.defects.Count() > 0)
            {
                de.form.SubmitDefect();
            }
            else
            {
                MessageBox.Show("Please be sure a Barcode and Defect Reason are entered.", "Defect Reason Error:");
            }

            Close();
        }
    }
}
