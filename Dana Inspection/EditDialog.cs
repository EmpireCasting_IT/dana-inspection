﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Text;
using gv = Dana_Inspection.GlobalVariables;
using gc = Dana_Inspection.GlobalOpClass;
using System.Windows.Forms;

namespace Dana_Inspection
{
    public partial class EditDialog : Form
    {
        public static EditDialog form;

        private DataTable DSource()
        {
            return gc.SQLData("Inspection_Proc @environment = '" + gc.AppEnvironment + "', @function = 'SELECT', @barcode = '', @dFlag = NULL, @dReason = NULL", null);
        }

        private void ReloadTable()
        {
            EditDataGrid.DataSource = DSource();
            EditDataGrid.Refresh();
        }

        private void ClearFields()
        {
            BarcodeTxtBox.Text = null;
            BarcodeTxtBox.ReadOnly = false;
            DefectCBox.Checked = false;
            ReasonComboBox.SelectedValue = "";
        }

        public EditDialog()
        {
            form = this;
            InitializeComponent();
        }

        private void EditDialog_Load(object sender, EventArgs e)
        {
            try
            {
                // Assign environment variables
                gc.myEnvironment();

                EditDataGrid.DataSource = DSource();

                ReasonComboBox.DataSource = gc.SQLData("SELECT Reason FROM BreakageTypes", null);
                ReasonComboBox.ValueMember = "Reason";
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Data Loading Error:");
                gc.ErrorLogger("DB", gc.AppName, err.Message, err.StackTrace);
            }
        }

        private void EditDataGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            BarcodeTxtBox.Text = EditDataGrid.Rows[e.RowIndex].Cells["Barcode"].Value.ToString();
            // Disable BarcodeTxtBox, so User can not Edit entry.
            BarcodeTxtBox.ReadOnly = true;
            DefectCBox.Checked = bool.Parse(EditDataGrid.Rows[e.RowIndex].Cells["DefectFlag"].Value.ToString());
            ReasonComboBox.SelectedValue = EditDataGrid.Rows[e.RowIndex].Cells["DefectReasons"].Value.ToString();
        }

        private void BarcodeTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (BarcodeTxtBox.ReadOnly == false)
            {
                EditDataGrid.DataSource = gc.SQLData("Inspection_Proc @environment = '" + gc.AppEnvironment + "', @function = 'SELECT', @barcode = '" + BarcodeTxtBox.Text + "', @dFlag = NULL, @dReason = NULL", null);
                EditDataGrid.Refresh();
            }
        }

        private void DefectCBox_CheckedChanged(object sender, EventArgs e)
        {
            if (DefectCBox.Checked == false)
            {
                ReasonComboBox.SelectedValue = "";
            }
        }

        private void ClearBttn_Click(object sender, EventArgs e)
        {
            ClearFields();
            ReloadTable();
        }

        private void SaveBttn_Click(object sender, EventArgs e)
        {
            try
            {
                if (DefectCBox.Checked == true && ReasonComboBox.SelectedValue != null || DefectCBox.Checked == false && ReasonComboBox.SelectedValue == null)
                {
                    gc.SQLString("EXEC Inspection_Proc @environment = " + gc.AppEnvironment + ",@function = 'UPDATE', @barcode = '" + BarcodeTxtBox.Text + "', @dFlag = '" + DefectCBox.Checked + "', @dReason = '" + ReasonComboBox.SelectedValue + "'", null);

                    ReloadTable();
                }
                else
                {
                    MessageBox.Show("Please enter a valid defect entry.", "Invalid Entry:");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message, "Saving Error:");
                gc.ErrorLogger("DB", gc.AppName, err.Message, err.StackTrace);
            }

            // Enable and clear fields, so it allows for searching records.
            ClearFields();
        }
    }
}
