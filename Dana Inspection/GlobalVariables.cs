﻿using System.Configuration;
using uc = Dana_Inspection.UserCode;

namespace Dana_Inspection
{
    public class GlobalVariables
    {
        // DB Variables
        public static string[] passcodes = { uc.passcode[0] };
        public static string username;
        public static string InspectFields = ConfigurationManager.AppSettings["Db_Inspection_Columns"];
        public static string backupFile;

        // App Variables
        public static string environment = ConfigurationManager.AppSettings["Mode"] + "_";
        public static string barcode;
        public static int usageInterval = int.Parse(ConfigurationManager.AppSettings["LoginInterval"]);

        public static string stringParse(string path)
        {
            return !string.IsNullOrWhiteSpace(path) ?
                path.Contains(" ") && (!path.StartsWith("\"") && !path.EndsWith("\"")) ?
                    "\"" + path + "\"" : path :
                    string.Empty;
        }
    }
}
